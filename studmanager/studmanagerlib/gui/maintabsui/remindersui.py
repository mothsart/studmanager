#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    UI class to tack reminder records
"""

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox,
                             QGridLayout, QWidget, 
                             QTreeWidget, QTreeWidgetItem)
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import QDate


from studmanagerlib.functions import reverse_dict
from studmanagerlib.database import Reminder
from studmanagerlib.gui.functionsui import (
    SPushButtonAdd,
    SPushButtonEdit,
    SPushButtonDelete,
    SDateEdit,
    SLabelDate,
    SComboBoxHorse,
    SLabelHorse,
    SPlainTextEditComments,
    SLabelComments,
    SCheckBoxDone)

__all__ = ['WidgetReminderRecords', 'InputReminderRecord']


class WidgetReminderRecords(QWidget):
    "widget inside helath record tab"

    def __init__(self, studdb):
        QWidget.__init__(self)
        self.dtb = studdb

        # == TabReminderRecords tab ==
        but_add = SPushButtonAdd("Add Reminder record")
        but_add.clicked.connect(self.create_reminder_record)

        but_edit = SPushButtonEdit("Edit Reminder record")
        but_edit.clicked.connect(self.edit_reminder_record)

        but_delete = SPushButtonDelete("Delete Reminder record")
        but_delete.clicked.connect(self.delete_reminder_record)

        self.dataview = ListReminderRecord(self.dtb)
        self.dataview.doubleClicked.connect(self.edit_reminder_record)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(but_add, 0, 0)
        gridlayout.addWidget(but_edit, 0, 1)
        gridlayout.addWidget(but_delete, 0, 2)
        gridlayout.addWidget(self.dataview, 1, 0, 1, 3)

    def create_reminder_record(self):
        "create/edit a reminder record"
        reminder_record = None

        if self.dtb.table_horse.get_nb_horses() == 0:
            QMessageBox.warning(
                self, 'Error !',
                "You have to create at least one horse before adding reminder record.",
                QMessageBox.Ok)
        else:
            input_data = InputReminderRecord(self.dtb, reminder_record)

            if input_data.exec_() == QDialog.Accepted:
                date_reminder_record, horse, comments, is_done = input_data.get_infos()

                self.dtb.table_reminders.add_reminder(
                    date_reminder_record, horse.dtb_id, comments, is_done)
                self.dataview.refresh()
            else:
                pass

    def edit_reminder_record(self):
        reminder_record = self.dataview.get_selection()
        if reminder_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one reminder record to edit.",
                QMessageBox.Ok)
        else:
            input_data = InputReminderRecord(self.dtb, reminder_record)

            if input_data.exec_() == QDialog.Accepted:
                date_reminder_record, horse, comments, is_done = input_data.get_infos()

                self.dtb.table_reminders.update_reminder(
                    reminder_record.dtb_id, date_reminder_record, horse.dtb_id, comments, is_done)
                self.dataview.refresh()
            else:
                pass

    def delete_reminder_record(self):
        "delete reminder record"
        reminder_record = self.dataview.get_selection()
        if reminder_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one reminder record to delete.",
                QMessageBox.Ok)
        else:
            self.dtb.table_reminders.delete_reminder(
                reminder_record.dtb_id)
            self.dataview.refresh()

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        self.dataview.set_horse(self.horse)


class ListReminderRecord(QTreeWidget):
    def __init__(self, studb):
        QTreeWidget.__init__(self)
        self.dtb = studb
        
        self.setColumnCount(3)
        self.setHeaderLabels(["Date", "Horse", "Comments"])
        self.view_dict = {}
        self.refresh()

    def refresh(self):
        self.clear()
        self.view_dict = {}
        
        
#        for row, reminder_record in enumerate(
#                self.dtb.table_reminders.get_reminders()):
#            date_reminder_record = str(reminder_record.get_date())
#            horse_name = reminder_record.get_horse().get_name()
#            comments = reminder_record.get_comments()
#            data = QTreeWidgetItem(
#                [date_reminder_record, horse_name, comments])
#            self.addTopLevelItem(data)
#            self.view_dict[reminder_record.dtb_id] = row
        
        font_done = QtGui.QFont()
        font_done.setStrikeOut(True)
        # setFont(font_done)

        late_color = QtGui.QBrush(QtCore.Qt.red)
        # setForeground(late_color)
        
        row =0
        for i, reminder_record in enumerate(
                self.dtb.table_reminders.get_reminders_not_done()):
            date_reminder_record = str(reminder_record.get_date())
            horse_name = reminder_record.get_horse().get_name()
            comments = reminder_record.get_comments()
            data = QTreeWidgetItem(
                [date_reminder_record, horse_name, comments])
            
            if reminder_record.get_date() <= QDate.currentDate() and not (reminder_record.is_done()):
                data.setForeground(0, late_color)
                data.setForeground(1, late_color)
                data.setForeground(2, late_color)
                
            self.addTopLevelItem(data)
            self.view_dict[reminder_record.dtb_id] = row
            row+=1
            
        for i, reminder_record in enumerate(
                self.dtb.table_reminders.get_reminders_done()):
            date_reminder_record = str(reminder_record.get_date())
            horse_name = reminder_record.get_horse().get_name()
            comments = reminder_record.get_comments()
            data = QTreeWidgetItem(
                [date_reminder_record, horse_name, comments])
            
            data.setFont(0, font_done)
            data.setFont(1, font_done)
            data.setFont(2, font_done)

                
            self.addTopLevelItem(data)
            self.view_dict[reminder_record.dtb_id] = row
            row+=1
            

    def get_selection(self):
        row = self.currentIndex().row()
        if row == -1:
            return None
        else:
            id_reminder_record = reverse_dict(self.view_dict, row)
            return Reminder(self.dtb, id_reminder_record)

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        self.refresh()

        
class InputReminderRecord(QDialog):
    "dialog to create/edit reminder records"
#        self.test_widget=InputReminderRecord(dtb, hr)
#        if self.test_widget.exec_() == QDialog.Accepted:
#            print(self.test_widget.get_infos())

    def __init__(self, studdb, reminder_record=None):
        QDialog.__init__(self)
        self.setWindowTitle("Reminder Record")
        self.dtb = studdb

        # data to display
        if reminder_record is None:
            date = None
            horse = None
            comments = None
            is_done = False
        else:
            date = reminder_record.get_date()
            horse = reminder_record.get_horse()
            comments = reminder_record.get_comments()
            is_done = reminder_record.is_done()


        # widget
        date_label = SLabelDate()
        self.date_edit = SDateEdit(date)

        label = SLabelHorse()
        self.combobox = SComboBoxHorse(self.dtb, horse)

        comments_label = SLabelComments()
        self.plain_text_edit_comments = SPlainTextEditComments(comments)

        self.cb_done = SCheckBoxDone(is_done)

        button_box = QDialogButtonBox(self)
        button_box.setStandardButtons(
            QDialogButtonBox.Save | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        # layout
        gridlayout = QGridLayout(self)

        gridlayout.addWidget(date_label, 0, 0)
        gridlayout.addWidget(self.date_edit, 0, 1)

        gridlayout.addWidget(label, 1, 0)
        gridlayout.addWidget(self.combobox, 1, 1)

        gridlayout.addWidget(comments_label, 2, 0)
        gridlayout.addWidget(self.plain_text_edit_comments, 2, 1)
        
        gridlayout.addWidget(self.cb_done, 3, 1)

        gridlayout.addWidget(button_box, 4, 1)
        # self.exec_()

    def get_infos(self):
        "return infos from dialog"
        date_reminder_record = self.date_edit.get()
        horse = self.combobox.get_obj()
        comments = self.plain_text_edit_comments.get_comments()
        is_done = self.cb_done.isChecked()
        return date_reminder_record, horse, comments, is_done
