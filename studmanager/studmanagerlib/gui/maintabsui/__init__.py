#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
	contains widgets of each tabs
"""

#__version__ = "0.1"

from . import myhorsetab
#from core import proclamer
#from .MyHorseTab import *
from .contactsui import WidgetContact, TabsContact
from .dashboardui import WidgetDashboard, InputStudInfos
from .myhorsesui import WidgetMyHorses
from .remindersui import WidgetReminderRecords, InputReminderRecord
