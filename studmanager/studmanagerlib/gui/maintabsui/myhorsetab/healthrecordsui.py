#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    UI class to tack health records
"""

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox,
                             QGridLayout, QWidget, 
                             QTreeWidget, QTreeWidgetItem)


from studmanagerlib.functions import reverse_dict
from studmanagerlib.database import HealthRecord
from studmanagerlib.gui.functionsui import (
    SPushButtonAdd,
    SPushButtonEdit,
    SPushButtonDelete,
    SDateEdit,
    SLabelDate,
    SComboBoxVet,
    SLabelVet,
    SPlainTextEditComments,
    SLabelComments)

__all__ = ['WidgetHealthRecords', 'InputHealthRecord']


class WidgetHealthRecords(QWidget):
    "widget inside helath record tab"

    def __init__(self, studdb, horse = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        # == TabHealthRecords tab ==
        but_add = SPushButtonAdd("Add Health record")
        but_add.clicked.connect(self.create_health_record)

        but_edit = SPushButtonEdit("Edit Health record")
        but_edit.clicked.connect(self.edit_health_record)

        but_delete = SPushButtonDelete("Delete Health record")
        but_delete.clicked.connect(self.delete_health_record)

        self.dataview = ListHealthRecord(self.dtb, self.horse)
        self.dataview.doubleClicked.connect(self.edit_health_record)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(but_add, 0, 0)
        gridlayout.addWidget(but_edit, 0, 1)
        gridlayout.addWidget(but_delete, 0, 2)
        gridlayout.addWidget(self.dataview, 1, 0, 1, 3)

    def create_health_record(self):
        "create/edit a health record"
        health_record = None

        if self.dtb.table_contact.get_nb_vets() == 0:
            QMessageBox.warning(
                self, 'Error !',
                "You have to create at least one vet before adding health record.",
                QMessageBox.Ok)
        else:

            input_data = InputHealthRecord(self.dtb, health_record)

            if input_data.exec_() == QDialog.Accepted:
                date_health_record, vet, comments = input_data.get_infos()

                self.dtb.table_health_records.add_health_record(
                    date_health_record, vet.dtb_id, self.horse.dtb_id, comments)
                self.dataview.refresh()
            else:
                pass

    def edit_health_record(self):
        health_record = self.dataview.get_selection()
        if health_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one health record to edit.",
                QMessageBox.Ok)
        else:
            input_data = InputHealthRecord(self.dtb, health_record)

            if input_data.exec_() == QDialog.Accepted:
                date_health_record, vet, comments = input_data.get_infos()

                self.dtb.table_health_records.update_health_record(
                    health_record.dtb_id, date_health_record, vet.dtb_id, self.horse.dtb_id, comments)
                self.dataview.refresh()
            else:
                pass

    def delete_health_record(self):
        "delete health record"
        health_record = self.dataview.get_selection()
        if health_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one health record to delete.",
                QMessageBox.Ok)
        else:
            self.dtb.table_health_records.delete_health_record(
                health_record.dtb_id)
            self.dataview.refresh()

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        self.dataview.set_horse(self.horse)


class ListHealthRecord(QTreeWidget):
    def __init__(self, studb, horse=None):
        QTreeWidget.__init__(self)
        self.dtb = studb
        self.horse = horse

        self.setColumnCount(3)
        self.setHeaderLabels(["Date", "Vet/Care taker", "Comments"])
        self.view_dict = {}
        self.set_horse(self.horse)

    def refresh(self):
        self.clear()
        self.view_dict = {}
        for row, health_record in enumerate(
                self.horse.get_health_records()):
            date_health_record = str(health_record.get_date())
            vet_name = health_record.get_vet().get_full_name()
            comments = health_record.get_comments()
            data = QTreeWidgetItem(
                [date_health_record, vet_name, comments])
            self.addTopLevelItem(data)
            self.view_dict[health_record.dtb_id] = row

    def get_selection(self):
        row = self.currentIndex().row()
        if row == -1:
            return None
        else:
            id_health_record = reverse_dict(self.view_dict, row)
            return HealthRecord(self.dtb, id_health_record)

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        if self.horse is not None:
            self.refresh()

        
class InputHealthRecord(QDialog):
    "dialog to create/edit health records"
#        self.test_widget=InputHealthRecord(dtb, hr)
#        if self.test_widget.exec_() == QDialog.Accepted:
#            print(self.test_widget.get_infos())

    def __init__(self, studdb, health_record=None):
        QDialog.__init__(self)
        self.setWindowTitle("Health Record")
        self.dtb = studdb

        # data to display
        if health_record is None:
            date = None
            vet = None
            comments = None
        else:
            date = health_record.get_date()
            vet = health_record.get_vet()
            comments = health_record.get_comments()

        # widget
        date_label = SLabelDate()
        self.date_edit = SDateEdit(date)

        label = SLabelVet()
        self.combobox = SComboBoxVet(self.dtb, vet)

        comments_label = SLabelComments()
        self.plain_text_edit_comments = SPlainTextEditComments(comments)

        button_box = QDialogButtonBox(self)
        button_box.setStandardButtons(
            QDialogButtonBox.Save | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        # layout
        gridlayout = QGridLayout(self)

        gridlayout.addWidget(date_label, 0, 0)
        gridlayout.addWidget(self.date_edit, 0, 1)

        gridlayout.addWidget(label, 1, 0)
        gridlayout.addWidget(self.combobox, 1, 1)

        gridlayout.addWidget(comments_label, 2, 0)
        gridlayout.addWidget(self.plain_text_edit_comments, 2, 1)

        gridlayout.addWidget(button_box, 3, 1)
        # self.exec_()

    def get_infos(self):
        "return infos from dialog"
        date_health_record = self.date_edit.get()
        vet = self.combobox.get_obj()
        comments = self.plain_text_edit_comments.get_comments()
        return date_health_record, vet, comments
