#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QPushButton
"""

from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QIcon

__all__ = [
    'SPushButtonAdd',
    'SPushButtonEdit',
    'SPushButtonDelete',
    'SPushButtonUpdate']

# QPushButton


class SPushButtonAdd(QPushButton):
    def __init__(self, title=""):
        QPushButton.__init__(self)
        self.setText(title)
        self.setIcon(QIcon.fromTheme("list-add"))


class SPushButtonEdit(QPushButton):
    def __init__(self, title=""):
        QPushButton.__init__(self)
        self.setText(title)
        self.setIcon(QIcon.fromTheme("document-properties"))


class SPushButtonDelete(QPushButton):
    def __init__(self, title=""):
        QPushButton.__init__(self)
        self.setText(title)
        self.setIcon(QIcon.fromTheme("list-remove"))


class SPushButtonUpdate(QPushButton):
    def __init__(self, title=""):
        QPushButton.__init__(self)
        self.setText(title)
        self.setIcon(QIcon.fromTheme("view-refresh"))
