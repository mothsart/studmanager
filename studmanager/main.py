#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Script to start StudManager
"""

import sys
import os

from PyQt5 import QtWidgets
from PyQt5.Qt import PYQT_VERSION_STR

from studmanagerlib.gui import StudWindow
from studmanagerlib.constants import ICON_PATH, LICENSE_PATH

import studmanagerlib
import sqlite3
import platform

#from studmanagerlib.database import StudDb

__all__ = ['studmanager']


def studmanager():
    "starts Stud Manager"

    print("Welcome in Stud Manager !")
    print("==================================================================")
    print("Script name : ", os.path.basename(__file__))
    print("Directory name : ", os.path.dirname(__file__))
    print("Path : ", os.path.abspath(__file__))
    print("==================================================================")
    print("= OS")
    print(platform.platform())
    print("==================================================================")
    print("= Python :")
    print(sys.version)
    print("= studmanagerlib : ")
    print(studmanagerlib.__version__)
    print("= PyQt5 : ")
    print(PYQT_VERSION_STR)
    print("= sqlite3 : ")
    print(sqlite3.version)
    print("==================================================================")
    # PYTHONPATH
    print("= PYTHONPATH")
    print("\n".join(sys.path))
    print("==================================================================")
    print("ICON PATH : ", ICON_PATH)
    print("LICENSE PATH : ", LICENSE_PATH)
    print("==================================================================")
    print("Starting user interface")

    APP = QtWidgets.QApplication(sys.argv)
    MAINWIN = StudWindow()
    MAINWIN.show()
    sys.exit(APP.exec_())


#    APP = QtWidgets.QApplication(sys.argv)
#    MAINWIN = StudWindow()
#    MAINWIN.show()
#    sys.exit(APP.exec_())

if __name__ == "__main__":
    studmanager()

    # PYTHONPATH
    # directory=os.path.dirname(os.path.abspath(__file__))
    ##    sys.path.insert(0, directory)


#    filename="sample/mydb.sqlite"
#    bdd=StudDb(filename)
#
#
#
#    bdd.add_horse("toto", 1, 1, "2013-05-12", 1)
#    bdd.add_horse("tata", 1, 2, "2009-05-10", 2)
#    bdd.add_horse("tutu", 2, 1, "2012-05-12", 3)
#    h=Horse(bdd,1)
#    print(h)
#    #h.update("yop", 2, 2, "2009-05-10", 2)
#    print(h)
#
#    bdd.add_contact("Dupont", "Jean", "la rue des rosiers", "06000000", "truc@yopmail.com",0,1)
#    bdd.add_contact("Pontdu", "Pierre", "la rue des rosiers", "06000000", "truc@yopmail.com",1,0)
#    bdd.add_contact("Pontdu", "Arthur", "la rue des rosiers \n91760 Itt",/
#     "06000000", "truc@yopmail.com",1,1)
#    bdd.add_contact("Wayne", "John", "la rue des rosiers", "06000000", "truc@yopmail.com",0,0)
#    c=Contact(bdd,1)
#    print(c)
#    #c.update("Hercule", "bla", "bla", "03000", "dsdsd @dsdsd.fr",1,1)
#    print(c)
#
#    bdd.add_healthrecords("2018-05-01",1,1,"vermifuge \nvaccin")
#    bdd.add_healthrecords("2018-05-03",2,1,"shampoo")
#
#    hr=HealthRecord(bdd,1)
#    print(hr)
#
#
#    bdd.conn.commit()
#    bdd.close()
