#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Script to start StudManager
"""

import sys
import os

from PyQt5 import QtWidgets
from PyQt5.Qt import PYQT_VERSION_STR

from studmanagerlib.gui import StudWindow
from studmanagerlib.constants import ICON_PATH, LICENSE_PATH

import studmanagerlib
import sqlite3
import platform

from studmanagerlib.database import StudDb, Breed, Color, Sex, Horse, Contact

#from studmanagerlib.database import StudDb

import sys
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, QComboBox, QDateEdit, QPlainTextEdit, QCheckBox, QPushButton
from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt, QDate
from PyQt5.QtGui import QIcon, QStandardItemModel

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox, QLabel,
                             QPlainTextEdit, QGridLayout, QWidget, QDateEdit,
                             QPushButton, QComboBox, QTreeView)

from PyQt5.QtGui import QIcon, QStandardItemModel
from PyQt5.QtCore import QDate, Qt

from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem, QApplication, QWidget
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem
from studmanagerlib.gui.functionsui import (
    SPushButtonAdd,
    SPushButtonEdit,
    SPushButtonDelete,
    SDateEdit,
    SLabelDate,
    SComboBoxVet,
    SLabelVet,
    SPlainTextEditComments,
    SLabelComments)

#from studmanagerlib.gui.maintabsui.myhorsetab import InputHealthRecord
from studmanagerlib.database import HealthRecord
from studmanagerlib.functions import reverse_dict
from studmanagerlib.gui.maintabsui import WidgetReminderRecords, WidgetDashboard
from studmanagerlib.gui.maintabsui.myhorsesui import WidgetDescriptionHorse, WidgetHealthRecords, WidgetMovementRecords, WidgetFarrierRecords
from studmanagerlib.gui.maintabsui.myhorsesui import TabMyHorse, ListHorse, WidgetMyHorses
from studmanagerlib.gui.maintabsui.contactsui import ListContact, WidgetDescriptionContact, WidgetContact

class HelloWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)


##        self.setMinimumSize(QSize(640, 480))
##        self.setWindowTitle("Hello world")

        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)

        gridLayout = QGridLayout(self)
        centralWidget.setLayout(gridLayout)

        dtb = StudDb("sample/demo.sqlite")
        hr = HealthRecord(dtb, 1)
        horse = Horse(dtb, 2)
        contact = Contact(dtb,1)

# breed=Breed(dtb,10)
##        test_widget = SComboBoxBreed(dtb, breed)

# color=Color(dtb,10)
##        test_widget = SComboBoxColor(dtb, color)

# sex=Sex(dtb,2)
##        test_widget = SComboBoxSex(dtb, sex)

# horse=Horse(dtb,3)
##        test_widget = SComboBoxHorse(dtb, horse)

# farrier=Contact(dtb,2)
##        test_widget = SComboBoxContact(dtb, farrier)

# test_widget=SPushButtonAdd("test")

#        self.test_widget=InputHealthRecord(dtb,hr)
#        if self.test_widget.exec_() == QDialog.Accepted:
#            print(self.test_widget.get_infos())

        #test_widget= ViewHealthRecord(dtb,horse )

        
        #test_widget = WidgetDescriptionHorse(dtb, horse)
        #test_widget = WidgetHealthRecords(dtb, horse)
        #test_widget = WidgetMovementRecords(dtb, horse)
        #test_widget = WidgetFarrierRecords(dtb, horse)
        
        test_widget = TabMyHorse(dtb, horse)
        #test_widget = ListHorse(dtb)
        #test_widget = WidgetMyHorses(dtb, horse)
        #test_widget = ListContact(dtb)
        #test_widget = WidgetDescriptionContact(dtb, contact)
        #test_widget = WidgetContact(dtb,contact)
        #test_widget = WidgetReminderRecords(dtb)
        #test_widget = WidgetDashboard(dtb)
        

        gridLayout.addWidget(test_widget)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = HelloWindow()
    mainWin.show()
    sys.exit(app.exec_())
