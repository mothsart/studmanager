#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class UI to track farrier record
"""

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox,
                             QGridLayout, QWidget, 
                             QTreeWidget, QTreeWidgetItem)


from studmanagerlib.functions import reverse_dict
from studmanagerlib.database import FarrierRecord
from studmanagerlib.gui.functionsui import (
    SPushButtonAdd,
    SPushButtonEdit,
    SPushButtonDelete,
    SDateEdit,
    SLabelDate,
    SComboBoxFarrier,
    SLabelFarrier,
    SPlainTextEditComments,
    SLabelComments)

__all__ = ['WidgetFarrierRecords', 'InputFarrierRecord']


class WidgetFarrierRecords(QWidget):
    "widget inside helath record tab"

    def __init__(self, studdb, horse = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        # == TabHealthRecords tab ==
        but_add = SPushButtonAdd("Add Farrier record")
        but_add.clicked.connect(self.create_farrier_record)

        but_edit = SPushButtonEdit("Edit Farrier record")
        but_edit.clicked.connect(self.edit_farrier_record)

        but_delete = SPushButtonDelete("Delete Farrier record")
        but_delete.clicked.connect(self.delete_farrier_record)

        self.dataview = ListFarrierRecord(self.dtb)
        self.dataview.doubleClicked.connect(self.edit_farrier_record)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(but_add, 0, 0)
        gridlayout.addWidget(but_edit, 0, 1)
        gridlayout.addWidget(but_delete, 0, 2)
        gridlayout.addWidget(self.dataview, 1, 0, 1, 3)
        
        self.set_horse(self.horse)

    def create_farrier_record(self):
        "create/edit a farrier record"
        farrier_record = None

        if self.dtb.table_contact.get_nb_farriers() == 0:
            QMessageBox.warning(
                self, 'Error !',
                "You have to create at least one farrier before adding farrier record.",
                QMessageBox.Ok)
        else:

            input_data = InputFarrierRecord(self.dtb, farrier_record)

            if input_data.exec_() == QDialog.Accepted:
                date_farrier_record, farrier, comments = input_data.get_infos()

                self.dtb.table_farrier_records.add_farrier_record(
                    date_farrier_record, farrier.dtb_id, self.horse.dtb_id, comments)
                self.dataview.refresh()
            else:
                pass

    def edit_farrier_record(self):
        farrier_record = self.dataview.get_selection()
        if farrier_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one farrier record to edit.",
                QMessageBox.Ok)
        else:
            input_data = InputFarrierRecord(self.dtb, farrier_record)

            if input_data.exec_() == QDialog.Accepted:
                date_farrier_record, farrier, comments = input_data.get_infos()

                self.dtb.table_farrier_records.update_farrier_record(
                    farrier_record.dtb_id, date_farrier_record, farrier.dtb_id, self.horse.dtb_id, comments)
                self.dataview.refresh()
            else:
                pass

    def delete_farrier_record(self):
        "delete farrier record"
        farrier_record = self.dataview.get_selection()
        if farrier_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one farrier record to delete.",
                QMessageBox.Ok)
        else:
            self.dtb.table_farrier_records.delete_farrier_record(
                farrier_record.dtb_id)
            self.dataview.refresh()
            
    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        if self.horse is not None:
            self.dataview.set_horse(self.horse)

class ListFarrierRecord(QTreeWidget):
    def __init__(self, studb, horse=None):
        QTreeWidget.__init__(self)
        self.dtb = studb
        self.horse = horse

        self.setColumnCount(3)
        self.setHeaderLabels(["Date", "Farrier", "Comments"])
        self.view_dict = {}
        self.set_horse(self.horse)

    def refresh(self):
        self.clear()
        self.view_dict = {}
        for row, farrier_record in enumerate(
                self.horse.get_farrier_records()):
            date_farrier_record = str(farrier_record.get_date())
            farrier_name = farrier_record.get_farrier().get_full_name()
            comments = farrier_record.get_comments()
            data = QTreeWidgetItem(
                [date_farrier_record, farrier_name, comments])
            self.addTopLevelItem(data)
            self.view_dict[farrier_record.dtb_id] = row

    def get_selection(self):
        row = self.currentIndex().row()
        if row == -1:
            return None
        else:
            id_farrier_record = reverse_dict(self.view_dict, row)
            return FarrierRecord(self.dtb, id_farrier_record)

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        if self.horse is not None:
            self.refresh()
        
class InputFarrierRecord(QDialog):
    "dialog to create/edit farrier records"
#        self.test_widget=InputHealthRecord(dtb, hr)
#        if self.test_widget.exec_() == QDialog.Accepted:
#            print(self.test_widget.get_infos())

    def __init__(self, studdb, farrier_record=None):
        QDialog.__init__(self)
        self.setWindowTitle("Farrier Record")
        self.dtb = studdb

        # data to display
        if farrier_record is None:
            date = None
            farrier = None
            comments = None
        else:
            date = farrier_record.get_date()
            farrier = farrier_record.get_farrier()
            comments = farrier_record.get_comments()

        # widget
        date_label = SLabelDate()
        self.date_edit = SDateEdit(date)

        label = SLabelFarrier()
        self.combobox = SComboBoxFarrier(self.dtb, farrier)

        comments_label = SLabelComments()
        self.plain_text_edit_comments = SPlainTextEditComments(comments)

        button_box = QDialogButtonBox(self)
        button_box.setStandardButtons(
            QDialogButtonBox.Save | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        # layout
        gridlayout = QGridLayout(self)

        gridlayout.addWidget(date_label, 0, 0)
        gridlayout.addWidget(self.date_edit, 0, 1)

        gridlayout.addWidget(label, 1, 0)
        gridlayout.addWidget(self.combobox, 1, 1)

        gridlayout.addWidget(comments_label, 2, 0)
        gridlayout.addWidget(self.plain_text_edit_comments, 2, 1)

        gridlayout.addWidget(button_box, 3, 1)
        # self.exec_()

    def get_infos(self):
        "return infos from dialog"
        date_farrier_record = self.date_edit.get()
        farrier = self.combobox.get_obj()
        comments = self.plain_text_edit_comments.get_comments()
        return date_farrier_record, farrier, comments
