#!/usr/bin/env python3
"""
 contains class and function for the contacts tab
"""

from PyQt5.QtWidgets import (QLineEdit, QWidget, QGridLayout)



from studmanagerlib.gui.functionsui import (SLabelLastName, SLabelFirstName,
                                            SLabelOrganization, SLabelEmail, 
                                            SLabelTel, SLabelAddress, SLabelJob,
                                            SCheckBoxFarrier, SCheckBoxVet, SPushButtonUpdate,
                                            SPlainTextEditComments)


__all__ = ['WidgetDescriptionContact']



# SLabelLastName, SLabelFirstName, SLabelOrganization, SLabelEmail, SLabelTel, SLabelAddress, SLabelJob
class WidgetDescriptionContact(QWidget):
    "widget inside description contact tab"
    def __init__(self, studdb, contact = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.contact = contact
        
        # == Description tab ==
        label_last_name = SLabelLastName()
        self.line_last_name = QLineEdit()

        label_first_name = SLabelFirstName()
        self.line_first_name = QLineEdit()

        label_organization = SLabelOrganization()
        self.line_organization = QLineEdit()

        label_address = SLabelAddress()
        self.line_address = SPlainTextEditComments()

        label_email = SLabelEmail()
        self.line_email = QLineEdit()

        label_tel = SLabelTel()
        self.line_tel = QLineEdit()

        label_job = SLabelJob()
        self.cb_vet = SCheckBoxVet()
        self.cb_farrier = SCheckBoxFarrier()

        self.but_save_changes = SPushButtonUpdate("Update")
        self.but_save_changes.clicked.connect(self.save_changes)

        # layout
        gridlayout = QGridLayout(self)
        gridlayout.addWidget(label_last_name, 0, 0, 1, 1)
        gridlayout.addWidget(self.line_last_name, 0, 1, 1, 2)

        gridlayout.addWidget(label_first_name, 1, 0, 1, 1)
        gridlayout.addWidget(self.line_first_name, 1, 1, 1, 2)

        gridlayout.addWidget(label_organization, 2, 0, 1, 1)
        gridlayout.addWidget(self.line_organization, 2, 1, 1, 2)

        gridlayout.addWidget(label_email, 3, 0, 1, 1)
        gridlayout.addWidget(self.line_email, 3, 1, 1, 2)

        gridlayout.addWidget(label_tel, 4, 0, 1, 1)
        gridlayout.addWidget(self.line_tel, 4, 1, 1, 2)

        gridlayout.addWidget(label_address, 5, 0, 1, 1)
        gridlayout.addWidget(self.line_address, 5, 1, 1, 2)

        gridlayout.addWidget(label_job, 6, 0, 1, 1)
        gridlayout.addWidget(self.cb_vet, 6, 1, 1, 1)
        gridlayout.addWidget(self.cb_farrier, 6, 2, 1, 1)

        gridlayout.addWidget(self.but_save_changes, 7, 1, 1, 2)

# for i in range(4):
##            gridlayout_description.setColumnStretch(i, 1)
##
# for i in range(4):
##            gridlayout_description.setRowStretch(i, 1)
        
        self.set_contact(self.contact)
        
    def save_changes(self):
        "update data"

        last_name = self.line_last_name.text()
        first_name = self.line_first_name.text()
        organization = self.line_organization.text()
        email = self.line_email.text()
        tel = self.line_tel.text()
        address = self.line_address.toPlainText()
        # print(last_name,first_name,email,tel)
        # print(address)
        is_vet = self.cb_vet.isChecked()
        is_farrier = self.cb_farrier.isChecked()

        self.dtb.table_contact.update_contact(self.contact.dtb_id, last_name,
                                              first_name, address, tel, email,
                                              is_vet, is_farrier, organization)

    def set_contact(self, contact):
        "set contact data in the widget"
        self.contact = contact
        if self.contact is not None:
            # set contact in description tab
            self.line_last_name.setText(self.contact.get_last_name())
            self.line_first_name.setText(self.contact.get_first_name())
            self.line_organization.setText(self.contact.get_organization())
            self.line_address.setPlainText(self.contact.get_address())
            self.line_email.setText(self.contact.get_email())
            self.line_tel.setText(self.contact.get_tel())
            self.cb_vet.setChecked(self.contact.is_vet())
            self.cb_farrier.setChecked(self.contact.is_farrier())