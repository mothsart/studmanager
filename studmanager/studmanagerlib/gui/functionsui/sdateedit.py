#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QCheckBox
"""

from PyQt5.QtWidgets import QDateEdit
from PyQt5.QtCore import QDate

__all__ = ['SDateEdit']


# QDateEdit
class SDateEdit(QDateEdit):
    def __init__(self, date=None):
        QDateEdit.__init__(self)
        self.setCalendarPopup(True)
        self.set(date)

    def set(self, date):
        if date is not None:
            self.setDate(date)
        else:
            self.setDate(QDate.currentDate())

    def get(self):
        return self.date().toString("yyyy-MM-dd")
