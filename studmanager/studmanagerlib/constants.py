#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Store constant data
"""

import os


__all__ = ['ICON_DIR', 'ICON_PATH', 'LICENSE_PATH', 'DATA_PATH']


#self.script_dir = os.path.dirname(os.path.realpath(__file__))
#self.script_dir = os.path.curdir

script_dir = os.path.abspath(os.path.curdir)
ICON_DIR = script_dir + os.path.sep + "icons"
ICON_PATH = ICON_DIR + os.path.sep
LICENSE_PATH = os.path.normpath(script_dir + os.path.sep + '../LICENSE.txt')
DATA_PATH = script_dir + os.path.sep + "data" + os.path.sep
