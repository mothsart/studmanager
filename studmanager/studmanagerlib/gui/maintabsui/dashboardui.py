#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    contains class and function for dashboard tab
"""

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox, QLabel,
                             QGridLayout, QWidget)

from PyQt5.QtCore import Qt

from studmanagerlib.gui.functionsui import SPushButtonUpdate, SComboBoxContact


__all__ = ['WidgetDashboard', 'InputStudInfos']


class WidgetDashboard(QWidget):
    "class to handle what is inside dashboard tab"

    def __init__(self, studdb):
        QWidget.__init__(self)
        self.dtb = studdb

        self.my_org_text = QLabel("Stud Name")
        self.my_org_text.setAlignment(Qt.AlignCenter)
        
        self.statistic_text = QLabel("Statistic")
        self.statistic_text.setAlignment(Qt.AlignTop)
        
        self.stud_text = QLabel("Stud Infos")
        self.stud_text.setAlignment(Qt.AlignTop)

        self.but_set_infos = SPushButtonUpdate("Set Stud Infos")
        self.but_set_infos.clicked.connect(self.set_infos)

        # calendar
        #self.cal = QCalendarWidget(self)
        # self.cal.setGridVisible(True)
        # self.cal.setSelectedDate(QDate.currentDate())

        gridlayout = QGridLayout(self)


        gridlayout.addWidget(self.my_org_text, 0, 0, 1, 3)

        # self.gridlayout.addWidget(self.cal,1,0,1,1)

        gridlayout.addWidget(self.stud_text, 1, 0, 2, 1)
        gridlayout.addWidget(self.but_set_infos, 3, 0, 1, 2)

        gridlayout.addWidget(self.statistic_text, 1, 1, 4, 1)

        for i in range(2):
            gridlayout.setColumnStretch(i, 1)
        for i in range(4):
            gridlayout.setRowStretch(i, 1)

        self.update_infos()

    def set_infos(self):
        "to set infos to display about the stud"
        if self.dtb.table_contact.get_nb_contacts() == 0:
            QMessageBox.warning(
                self, 'Error !',
                "You have to add your contact account before setting Stud Infos.",
                QMessageBox.Ok)
        else:              
            try:
                manager = self.dtb.table_infos.get_manager()
                input_data = InputStudInfos(self.dtb, manager)
            except BaseException:
                input_data = InputStudInfos(self.dtb)
            
            if input_data.exec_() == QDialog.Accepted:
                manager = input_data.get_infos()
                self.dtb.table_infos.set_manager(manager)
                self.update_infos()

    def set_statistic_text(self):
        "to set statisctic text"
        nb_horse = self.dtb.table_horse.get_nb_horses()
        nb_horse_in = self.dtb.table_horse.get_nb_horses_in()

        nb_contact = self.dtb.table_contact.get_nb_contacts()
        nb_vets = self.dtb.table_contact.get_nb_vets()
        nb_farrier = self.dtb.table_contact.get_nb_farriers()

        nb_reminder = self.dtb.table_reminders.get_nb_reminders_not_done()
        nb_reminder_late = self.dtb.table_reminders.get_nb_reminders_late_not_done(
        )

        stat_text = """ <h1>Statistics</h1>
                      <h2>Horses</h2>
                      Nb Horses : {} <br/>
                      Nb Horses in : {} <br/>
                      <h2>Contacts</h2>
                      Nb Contacts : {} <br/>
                      Nb Vet/Care taker : {} <br/>
                      Nb Farrier : {} <br/>
                      <h2>Reminders</h2>
                      Nb Reminders : {} <br/>
                      Nb Reminders Late : {} <br/> """.format(
            nb_horse, nb_horse_in, nb_contact, nb_vets, nb_farrier,
            nb_reminder, nb_reminder_late)

        self.statistic_text.setText(stat_text)

    def set_stud_text(self):
        "to set stud text"
        # Manager=self.dtb.get_manager()
        try:
            manager = self.dtb.table_infos.get_manager()

            address = manager.get_address()
            name = manager.get_short_name()
            email = manager.get_email()
            tel = manager.get_tel()
            # print(Manager)
        except BaseException:
            address = ""
            name = ""
            email = ""
            tel = ""

        address_html = address.replace('\n', "<br/>")

        stud_text = """<h1>My Stud</h1>
                     <h2>Stud infos</h2>
                     Address :<br/>
                     {}<br/>
                     <h2>Manager</h2>
                     Name : {}<br/>
                     Email : {}<br/>
                     Tel : {}<br/>
                     """.format(address_html, name, email, tel)
        self.stud_text.setText(stud_text)

    def set_my_org_text(self):
        "to set organization title"
        try:
            manager = self.dtb.table_infos.get_manager()
            if manager.get_organization() != "":
                my_org_text = """<h1>{}</h1>""".format(
                    manager.get_organization())
            else:
                my_org_text = """<h1>My Stud !!!</h1>"""
        except BaseException:
            my_org_text = """<h1>My Stud !!!</h1>"""

        self.my_org_text.setText(my_org_text)

    def update_infos(self):
        "to update text in the tab"
        self.set_statistic_text()
        self.set_stud_text()
        self.set_my_org_text()


class InputStudInfos(QDialog):
    "dialog to choose who is the manager"

    def __init__(self, studdb, manager=None):
        QDialog.__init__(self)
        self.dtb = studdb

        self.setWindowTitle("Who am I ?")

        label_manager = QLabel("Manager : ")
        self.cb_manager = SComboBoxContact(self.dtb, manager)

        button_box = QDialogButtonBox(self)
        button_box.setStandardButtons(QDialogButtonBox.Save
                                           | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(label_manager, 1, 0)
        gridlayout.addWidget(self.cb_manager, 1, 1)

        gridlayout.addWidget(button_box, 2, 1)



    def get_infos(self):
        "return infos from this dialog"

        manager = self.cb_manager.get_obj()
        return manager
