#!/usr/bin/env python3
"""
 contains class and function for the contacts tab
"""

from PyQt5.QtWidgets import (QGridLayout, QWidget, QTabWidget,
                              QTreeWidget, QTreeWidgetItem)
from PyQt5.QtGui import QIcon


from studmanagerlib.functions import reverse_dict
from studmanagerlib.database import Contact
from studmanagerlib.gui.functionsui import SPushButtonAdd
from studmanagerlib.gui.maintabsui.contacttab import WidgetDescriptionContact


__all__ = ['WidgetContact', 'TabsContact']



class WidgetContact(QWidget):
    "widget inside contact tab"

    def __init__(self, studdb, contact = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.contact =  contact


        # == Contact page ==

        # #########
        # left side
        # #########
        self.but_add_contact = SPushButtonAdd("Add Contact")
        self.but_add_contact.clicked.connect(self.create_contact)
        
        self.dataview = ListContact(self.dtb)
        self.dataview.clicked.connect(self.on_contact_selected)

        # #########
        # right side
        # #########

        # Initialize tab screen
        self.tabs_contact = TabsContact(self.dtb, self.contact)
        self.tabs_contact.tab_description.but_save_changes.clicked.connect(self.dataview.refresh)

#        # == layout ==

        gridlayout = QGridLayout(self)
        # add left and right side in tabContact
        # add treeview and tabs to tabContact
        gridlayout.addWidget(self.but_add_contact, 0, 0, 1, 1)
        gridlayout.addWidget(self.dataview, 1, 0, 9, 3)
        gridlayout.addWidget(self.tabs_contact, 0, 3, 10, 7)
        for i in range(10):
            gridlayout.setColumnStretch(i, 1)

        
    def create_contact(self):
        "create contact"
        nb_contact = self.dtb.table_contact.get_nb_contacts()

        last_name = "New contact"
        first_name = "John Doe {}".format(str(nb_contact))
        address = ""
        tel = ""
        email = ""
        is_vet = 1
        is_farrier = 0
        organization = ""

        contact_id = self.dtb.table_contact.add_contact(
            last_name, first_name, address, tel, email, is_vet, is_farrier,
            organization)
        
        contact = Contact(self.dtb, contact_id)
        self.set_contact(contact)

    def on_contact_selected(self):
        "action when a contact is selected"
        contact = self.dataview.get_selection()
        self.set_contact(contact)

    def set_contact(self, contact):
        "update tabs"
        self.contact = contact
        self.tabs_contact.set_contact(self.contact)
        self.dataview.refresh()

class ListContact(QTreeWidget):
    def __init__(self, studb):
        QTreeWidget.__init__(self)
        self.dtb = studb

        self.setColumnCount(1)
        self.setHeaderLabels(["Name"])
       
        self.view_dict= {}
        
        self.refresh()

    def refresh(self):
            
        # to refresh the list        
        self.clear()
        self.view_dict= {}
          
        for row, contact in enumerate(self.dtb.table_contact.get_contacts()):
            data=QTreeWidgetItem([contact.get_full_name()])
            self.addTopLevelItem(data)
            self.view_dict[contact.dtb_id] = row
            
    def get_selection(self):
        row = self.currentIndex().row()  # row
        if row == -1:
            return None
        else:
            id_contact = reverse_dict(self.view_dict, row)
            contact = Contact(self.dtb, id_contact)
            return contact
        
class TabsContact(QTabWidget):
    "all contact tab"
    def __init__(self, studdb, contact = None):
        QTabWidget.__init__(self)
        self.dtb = studdb
        self.contact = contact

        # Initialize tab screen
        self.tab_description = WidgetDescriptionContact(self.dtb, self.contact)

        # Add tabs
        self.addTab(self.tab_description, "Description")
        # tab icon
        self.setTabIcon(0, QIcon.fromTheme("emblem-documents"))
    
    def set_contact(self,contact):
        self.contact =  contact
        if self.contact is not None:
            self.tab_description.set_contact(self.contact)
        else:
            self.hide() ## bug !!!
