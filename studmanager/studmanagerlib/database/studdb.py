#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class to handle relation between UI and stud database(sqlite)
"""

import sqlite3
import os.path

#from StudManagerLib.Database import Horse, Breed, Sex, Color
#from StudManagerLib.Database import Reminder
#from StudManagerLib.Database import Contact

from .horsedb import Horse, Breed, Sex, Color, MovementRecord, HealthRecord, FarrierRecord
from .reminderdb import Reminder
from .contactdb import Contact

from studmanagerlib.constants import DATA_PATH
import studmanagerlib


__all__ = ['StudDb']

# TableHealthRecords, TableFarrierRecords, TableMovementRecords
# TableColor, TableInfos, TableSex, TableBreed, TableReminders, TableHorse, TableContact


class StudDb():
    "main class to interact with database .sqlite"

    def __init__(self, filename):

        self.file = filename
        print(self.file)

        self.table_infos = TableInfos(self)
        self.table_horse = TableHorse(self)
        self.table_breed = TableBreed(self)
        self.table_color = TableColor(self)
        self.table_sex = TableSex(self)
        self.table_health_records = TableHealthRecords(self)
        self.table_farrier_records = TableFarrierRecords(self)
        self.table_movement_records = TableMovementRecords(self)
        self.table_reminders = TableReminders(self)
        self.table_contact = TableContact(self)

        if os.path.exists(self.file):
            print("File already exists")

            self.conn = sqlite3.connect(self.file)
            self.cur = self.conn.cursor()

        else:
            print("Creating database")
            self.conn = sqlite3.connect(self.file)
            self.cur = self.conn.cursor()

            self.create_new_db()

#        print("\n".join(self.__dict__))
#        print("\n")
#        print("\n".join(self.table_breed.__dict__))
#        print("\n")
#        print("\n".join(self.table_breed.db.__dict__))
# =========================================================================
# create new db
# =========================================================================

    def create_new_db(self):
        "create a new database"

        # infos
        self.table_infos.create_table_infos()
        self.table_infos.set_version(studmanagerlib.__version__)
        # horse tables
        self.table_horse.create_table_horse()
        self.table_breed.create_table_breed()
        self.table_color.create_table_color()
        self.table_sex.create_table_sex()

        self.table_health_records.create_table_health_records()
        self.table_farrier_records.create_table_farrier_records()
        self.table_movement_records.create_table_movement_record()

        # reminders
        self.table_reminders.create_table_reminders()

        # contact table
        self.table_contact.create_table_contact()
        # view
        self.create_view()

        self.init_dtb()

        self.conn.commit()
        print("Database created")

    def create_view(self):
        "create views"
        #  Horse
        self.cur.execute('''
            CREATE VIEW `My Horses` AS
            Select THorse.name, THorse.birthdate, TBreed.name, TColor.name, TSex.name from THorse
            left join TColor on TColor.id=THorse.id_color
            left join TSex on TSex.id=THorse.id_sex
            left join TBreed on TBreed.id=THorse.id_breed
            order by THorse.name;
                        ''')
        self.cur.execute('''
            CREATE VIEW `Horses number` AS
            SELECT COUNT(*) FROM (select THorse.name from THorse)
                        ''')
        self.cur.execute('''
            CREATE VIEW `Horse Details` AS
            select chev.name, chev.birthdate, TSex.name, TBreed.name , TColor.name from THorse as chev
            left join TBreed on chev.id_breed=TBreed.id
            left join TColor on chev.id_color=TColor.id
            left join TSex on chev.id_sex=TSex.id
            order by chev.name
                        ''')
        # contact
        self.cur.execute('''
            CREATE VIEW `Contact number` AS
            SELECT COUNT(*) FROM (select TContact.id from TContact)
                        ''')
        self.cur.execute('''
            CREATE VIEW `Contact Details` AS
            select TContact.last_name, TContact.first_name, TContact.organization, TContact.address, TContact.tel, TContact.email FROM TContact
            order by TContact.last_name
                        ''')

        # health records
        self.cur.execute('''
            CREATE VIEW `Health Records Details` AS
            select THealthRecords.date, TContact.last_name, TContact.first_name, TContact.organization, THorse.name, THealthRecords.comments FROM THealthRecords
            left join TContact on TContact.id=THealthRecords.id_vet
            left join THorse on THorse.id=THealthRecords.id_horse
            order by THealthRecords.date
                        ''')

        # farrier records
        self.cur.execute('''
            CREATE VIEW `Farrier Records Details` AS
            select TFarrierRecords.date, TContact.last_name, TContact.first_name,TContact.organization, THorse.name, TFarrierRecords.comments FROM TFarrierRecords
            left join TContact on TContact.id=TFarrierRecords.id_farrier
            left join THorse on THorse.id=TFarrierRecords.id_horse
            order by TFarrierRecords.date
                        ''')

        # movement records
        self.cur.execute('''
            CREATE VIEW `Movement Records Details` AS
            select TMovementRecords.date, THorse.name, TMovementRecords.comments FROM TMovementRecords
            left join THorse on THorse.id=TMovementRecords.id_horse
            order by TMovementRecords.date
                        ''')
        # reminders records
        self.cur.execute('''
            CREATE VIEW `Reminders Details` AS
            select TReminders.date, THorse.name, TReminders.comments, TReminders.is_done FROM TReminders
            left join THorse on THorse.id=TReminders.id_horse
            order by TReminders.date
                        ''')

        # self.conn.commit()

    def init_dtb(self):
        "add basic data (breed,sex,color) into the database"

        self.table_breed.init_dtb()
        self.table_sex.init_dtb()
        self.table_color.init_dtb()

    def save(self):
        "save database"
        self.conn.commit()

    def close(self):
        "close database"
        self.conn.close()


class TableHealthRecords():
    "class to interact with THealthRecords"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_health_records(self):
        "create THealthRecords"
        self.dtb.cur.execute('''
            CREATE TABLE `THealthRecords` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_vet` INTEGER,
              `id_horse` INTEGER,
              `comments`    TEXT
            );
                        ''')
        # self.conn.commit()

    # health records

    def add_health_record(self, date, id_vet, id_horse, comments):
        "add health record"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO THealthRecords (date,id_vet,id_horse,comments)
                                VALUES ( ?,?,?,? )''', (
                date,
                id_vet,
                id_horse,
                comments,
            ))
        id_health_record = self.dtb.cur.lastrowid
        print("Health record created")
        print(HealthRecord(self.dtb, id_health_record))
        # self.conn.commit()

    def update_health_record(
            self,
            id_health_record,
            date,
            id_vet,
            id_horse,
            comments):
        "update health record"
        self.dtb.cur.execute('''UPDATE THealthRecords
                             SET (date,id_vet,id_horse,comments) = (?,?,?,? )
                             WHERE THealthRecords.id=? ''', ((
            date,
            id_vet,
            id_horse,
            comments,
            id_health_record,
        )))
        print("Health record updated")
        print(HealthRecord(self.dtb, id_health_record))

    def delete_health_record(self, id_hr):
        "delete health record"
        self.dtb.cur.execute('''DELETE FROM THealthRecords WHERE id = ? ''',
                             ((id_hr, )))


class TableFarrierRecords():
    "class to interact with TFarrierRecords"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_farrier_records(self):
        "create TFarrierRecords"
        self.dtb.cur.execute('''
            CREATE TABLE `TFarrierRecords` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_farrier` INTEGER,
              `id_horse` INTEGER,
              `comments`    TEXT
            );
                        ''')
        # self.conn.commit()

    # farrier records

    def add_farrier_record(self, date, id_farrier, id_horse, comments):
        "add farrier records"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TFarrierRecords
                             (date,id_farrier,id_horse,comments)
                             VALUES ( ?,?,?,? )''', (
            date,
            id_farrier,
            id_horse,
            comments,
        ))
        id_farrier_record = self.dtb.cur.lastrowid
        print("Farrier record created")
        print(FarrierRecord(self.dtb, id_farrier_record))
        # self.conn.commit()

    def update_farrier_record(
            self,
            id_farrier_record,
            date,
            id_farrier,
            id_horse,
            comments):
        "update farrrier record"
        self.dtb.cur.execute('''UPDATE TFarrierRecords
                             SET (date,id_farrier,id_horse,comments) = (?,?,?,? )
                             WHERE TFarrierRecords.id=?  ''', ((
            date,
            id_farrier,
            id_horse,
            comments,
            id_farrier_record,
        )))
        print("Farrier record updated")
        print(FarrierRecord(self.dtb, id_farrier_record))

    def delete_farrier_record(self, id_farrier_record):
        "delete farrier record"
        self.dtb.cur.execute('''DELETE FROM TFarrierRecords WHERE id = ? ''',
                             ((id_farrier_record, )))


class TableMovementRecords():
    "class to interact with TMovementRecords"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_movement_record(self):
        "create TMovementRecords"
        self.dtb.cur.execute('''
            CREATE TABLE `TMovementRecords` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_horse` INTEGER,
              `comments`    TEXT
            );
                        ''')
        # self.conn.commit()

    # movement records

    def add_movement_record(self, date, id_horse, comments):
        "add movement record"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO TMovementRecords (date,id_horse,comments)
                VALUES ( ?,?,? )''', (
                date,
                id_horse,
                comments,
            ))
        id_movement_record = self.dtb.cur.lastrowid
        print("Movement record created")
        print(MovementRecord(self.dtb, id_movement_record))
        # self.conn.commit()

    def update_movement_record(
            self,
            id_movement_record,
            date,
            id_horse,
            comments):
        "update movement record"
        self.dtb.cur.execute('''UPDATE TMovementRecords
                             SET (date,id_horse,comments) = (?,?,? )
                             WHERE TMovementRecords.id=?  ''', ((
            date,
            id_horse,
            comments,
            id_movement_record,
        )))
        print("Movement updated")
        print(MovementRecord(self.dtb, id_movement_record))

    def delete_movement_record(self, id_movement_record):
        "delete movement"
        self.dtb.cur.execute('''DELETE FROM TMovementRecords WHERE id = ? ''',
                             ((id_movement_record, )))


class TableColor():
    "class to interact with TSex"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data into TColor"
        # color
        file = open(DATA_PATH + "ColorList.txt", "r")
        for line in file.readlines():
            color = line.replace('\n', '')
            self.add_color(color)

        file.close()

    def create_table_color(self):
        "create TColor"
        self.dtb.cur.execute('''
            CREATE TABLE `TColor` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_color(self, color):
        "add color"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TColor (name)
                VALUES ( ? )''', (color, ))
        # self.conn.commit()

    # =========================================================================
    # colors informations
    # =========================================================================

    def _get_list_id_colors(self):
        "return colors id"
        self.dtb.cur.execute(
            "select TColor.id From TColor ORDER BY TColor.name")
        list_id_colors = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_colors]

    def get_colors(self):
        "return colors"
        list_id_colors = self._get_list_id_colors()
        return [Color(self.dtb, i) for i in list_id_colors]


class TableInfos():
    "class to interact with TInfos"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_infos(self):
        "create TInfos"
        self.dtb.cur.execute('''
            CREATE TABLE `TInfos` (
                `id`    INTEGER PRIMARY KEY CHECK (id = 0),
                `id_manager`    INTEGER,
                `version`    TEXT
            );
                        ''')
        # self.conn.commit()

    # =========================================================================
    # infos
    # =========================================================================

    def set_manager(self, manager):
        "set manager id"
        # self.cur.execute('''UPDATE TInfos SET (id_manager)
        # = (?) WHERE TInfos.id=0  ''', ( (id_manager, )))
        id_manager = manager.dtb_id
        self.dtb.cur.execute('''insert or replace into TInfos (id,id_manager)
                    values (?,?) ''', ((
            0,
            id_manager,
        )))
        print("New Manager id : ", id_manager)
        print(manager)

    def get_manager(self):
        "return manager"
        self.dtb.cur.execute(
            'SELECT id_manager FROM TInfos WHERE TInfos.id=0 ')
        id_manager = self.dtb.cur.fetchone()[0]
        return Contact(self.dtb, id_manager)

    def set_version(self, version):
        "set version"
        self.dtb.cur.execute('''insert or replace into TInfos (id,version)
                    values (?,?) ''', ((
            0,
            version,
        )))

    def get_version(self):
        "return manager"
        self.dtb.cur.execute(
            'SELECT version FROM TInfos WHERE TInfos.id=0 ')
        version = self.dtb.cur.fetchone()[0]
        return version

class TableSex():
    "class to interact with TSex"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data int TSex"
        # sex
        file = open(DATA_PATH + "SexList.txt", "r")
        for line in file.readlines():
            sex = line.replace('\n', '')
            self.add_sex(sex)

        file.close()

    def create_table_sex(self):
        "create TSex"
        self.dtb.cur.execute('''
            CREATE TABLE `TSex` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_sex(self, sex):
        "add sex"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TSex (name)
                VALUES ( ? )''', (sex, ))
        # self.conn.commit()

    # =========================================================================
    # sex informations
    # =========================================================================

    def _get_list_id_sexes(self):
        "return sexes id"
        self.dtb.cur.execute("select TSex.id From TSex ORDER BY TSex.name")
        list_id_sexes = self.dtb.cur.fetchall()
        # self.dtb.conn.commit()
        return [item[0] for item in list_id_sexes]

    def get_sexes(self):
        "return sexes"
        list_id_sexes = self._get_list_id_sexes()
        return [Sex(self.dtb, i) for i in list_id_sexes]


class TableBreed():
    "class to interact with TBreed"

    def __init__(self, dtb):
        self.dtb = dtb

    def init_dtb(self):
        "add basic data init TBreed"
        # breed
        file = open(DATA_PATH + "BreedList.txt", "r")
        for line in file.readlines():
            breed = line.replace('\n', '')
            self.add_breed(breed)

        file.close()

    def create_table_breed(self):
        "create TBreed"
        self.dtb.cur.execute('''
            CREATE TABLE `TBreed` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE
            );
                        ''')
        # self.conn.commit()

    def add_breed(self, breed):
        "add breed"
        self.dtb.cur.execute('''INSERT OR IGNORE INTO TBreed (name)
                VALUES ( ? )''', (breed, ))
        # self.conn.commit()

    # =========================================================================
    # breeds informations
    # =========================================================================

    def _get_list_id_breeds(self):
        "return breed id"
        self.dtb.cur.execute(
            "select TBreed.id From TBreed ORDER BY TBreed.name")
        list_id_breeds = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_breeds]

    def get_breeds(self):
        "return breeds"
        list_id_breeds = self._get_list_id_breeds()
        return [Breed(self.dtb, i) for i in list_id_breeds]


class TableReminders():
    "class to interact with TReminders"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_reminders(self):
        "create TReminders"
        self.dtb.cur.execute('''
            CREATE TABLE `TReminders` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `date`    DATE,
              `id_horse` INTEGER,
              `comments`    TEXT,
              `is_done`         INTEGER
            );
                        ''')
        # self.conn.commit()

    # =========================================================================
    # reminders
    # =========================================================================
    # reminders

    def add_reminder(self, date, id_horse, comments, is_done):
        "add reminder"
        self.dtb.cur.execute(
            '''INSERT OR IGNORE INTO TReminders (date,id_horse,comments,is_done)
                VALUES ( ?,?,?,? )''', (
                date,
                id_horse,
                comments,
                int(is_done),
            ))
        id_reminder = self.dtb.cur.lastrowid
        print("Reminder record created")
        print(Reminder(self.dtb, id_reminder))
        # self.conn.commit()

    def update_reminder(self, id_reminder, date, id_horse, comments, is_done):
        "update reminder"
        self.dtb.cur.execute('''UPDATE TReminders
                             SET (date,id_horse,comments,is_done) = (?,?,?,? )
                             WHERE TReminders.id=?  ''', ((
            date,
            id_horse,
            comments,
            int(is_done),
            id_reminder,
        )))
        print("Reminder record updated")
        print(Reminder(self.dtb, id_reminder))

    def delete_reminder(self, id_reminder):
        "delete reminder"
        self.dtb.cur.execute('''DELETE FROM TReminders WHERE id = ? ''',
                             ((id_reminder, )))

    # =========================================================================
    # reminder informations
    # =========================================================================

    def get_reminders(self):
        "return all reminders"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_reminders_done(self):
        "return reminders done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=1
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_reminders_not_done(self):
        "return reminders not done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=0
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_nb_reminders_not_done(self):
        "return number of reminders not done"
        return len(self.get_reminders_not_done())

    def get_reminders_late_not_done(self):
        "retunr reminders late and not done"
        self.dtb.cur.execute('''select TReminders.id from TReminders
                        WHERE TReminders.is_done=0 and TReminders.date<=date('now')
                         ORDER BY TReminders.date ASC''')
        list_id_fr = self.dtb.cur.fetchall()
        return [Reminder(self.dtb, item[0]) for item in list_id_fr]

    def get_nb_reminders_late_not_done(self):
        "return number of reminders late and not done"
        return len(self.get_reminders_late_not_done())


class TableHorse():
    "class to interact with THorse"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_horse(self):
        "create THorse"
        self.dtb.cur.execute('''
            CREATE TABLE `THorse` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `name`    TEXT UNIQUE,
                `id_breed`    INTEGER,
                `id_color`    INTEGER,
                `id_sex`    INTEGER,
                `birthdate`    DATE
            );
                        ''')
        # self.conn.commit()

    def add_horse(self, name, id_sex, id_color, birthdate, id_breed):
        "add horse"
        if name == "":
            print("Name is empty !")
            return None
        else:
            self.dtb.cur.execute('''INSERT OR IGNORE INTO THorse
                                 (name,id_breed, id_color,id_sex,birthdate)
                                 VALUES (?,?,?,?,? )''', ((
                name,
                id_breed,
                id_color,
                id_sex,
                birthdate,
            )))

            # self.conn.commit()
            id_horse = self.dtb.cur.lastrowid
            print("New horse created")
            print(Horse(self.dtb, id_horse))
            return id_horse

    def update_horse(self, id_horse, name, id_sex, id_color, birthdate,
                     id_breed):
        "update horse infos"
        if name == "":
            print("Name is empty !")
        else:
            self.dtb.cur.execute(
                '''UPDATE THorse
                                 SET (name,id_breed, id_color,id_sex,birthdate)
                                 = (?,?,?,?,? )
                                 WHERE THorse.id=? ''',
                ((name, id_breed, id_color, id_sex, birthdate, id_horse)))
            print("Horse updated")
            print(Horse(self.dtb, id_horse))
            # self.conn.commit()

    # =========================================================================
    # horses informations
    # =========================================================================

    def _get_list_id_horses(self):
        "return horses id"
        self.dtb.cur.execute(
            "select THorse.id From THorse ORDER BY THorse.name")
        list_id_horses = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_horses]

    def get_horses(self):
        "return horses"
        list_id_horses = self._get_list_id_horses()
        return [Horse(self.dtb, i) for i in list_id_horses]

    def get_nb_horses(self):
        "return number of horses"
        return len(self.get_horses())

    def get_horses_in(self):
        "return horses in the stud"
        list_horses_in = []
        for horse in self.get_horses():
            if bool(horse.is_in()):
                list_horses_in.append(horse)
        return list_horses_in

    def get_nb_horses_in(self):
        "return number of horses in the stud"
        return len(self.get_horses_in())


class TableContact():
    "class to interact with TContact"

    def __init__(self, dtb):
        self.dtb = dtb

    def create_table_contact(self):
        "create TContact"
        self.dtb.cur.execute('''
            CREATE TABLE `TContact` (
                `id`    INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                `last_name`    TEXT,
                `first_name`    TEXT,
                `address`    TEXT,
                `tel`            TEXT,
                `email`     TEXT,
                `is_vet`         INTEGER,
                `is_farrier`     INTEGER,
                `organization`    TEXT,
                UNIQUE (last_name,first_name)

            );
                        ''')
        # self.conn.commit()

    # contact
    def add_contact(self, last_name, first_name, address, tel, email, is_vet,
                    is_farrier, organization):
        "add contact"
        if last_name == "" or first_name == "":
            print("Name and first name are empty !")
            return None
        else:
            self.dtb.cur.execute('''INSERT OR IGNORE INTO
                                 TContact (last_name, first_name, address, tel,
                                 email, is_vet, is_farrier,organization)
                                 VALUES (?,?,?,?,?,?,?,? )''', ((
                last_name,
                first_name,
                address,
                tel,
                email,
                int(is_vet),
                int(is_farrier),
                organization,
            )))

            # self.conn.commit()
            id_contact = self.dtb.cur.lastrowid
            print("New contact created")
            print(Contact(self.dtb, id_contact))
            return id_contact

    def update_contact(self, id_contact, last_name, first_name, address, tel,
                       email, is_vet, is_farrier, organization):
        "update contact infos"
        if last_name == "" or first_name == "":
            print("Name and first name are empty !")
        else:
            self.dtb.cur.execute('''UPDATE TContact
                SET (last_name,first_name, address,tel,email, is_vet, is_farrier,organization)
                    = (?,?,?,?,?,?,?,? ) WHERE TContact.id=? ''', ((
                last_name,
                first_name,
                address,
                tel,
                email,
                int(is_vet),
                int(is_farrier),
                organization,
                id_contact,
            )))

            print("Contact updated")
            print(Contact(self.dtb, id_contact))

        # self.conn.commit()

    # =========================================================================
    # contact informations
    # =========================================================================

    def _get_list_id_contacts(self):
        "return contacts id"
        self.dtb.cur.execute(
            "select TContact.id From TContact ORDER BY TContact.last_name")
        list_id_contacts = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_contacts]

    def get_contacts(self):
        "return contacts"
        list_id_contacts = self._get_list_id_contacts()
        return [Contact(self.dtb, i) for i in list_id_contacts]

    def get_nb_contacts(self):
        "return number of contacts"
        return len(self.get_contacts())

    def _get_list_id_vets(self):
        "return vets id"
        self.dtb.cur.execute(
            "select TContact.id From TContact where TContact.is_vet=1 ORDER BY TContact.last_name"
        )
        list_id_vets = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_vets]

    def get_vets(self):
        "return vets"
        list_id_vets = self._get_list_id_vets()
        return [Contact(self.dtb, i) for i in list_id_vets]

    def get_nb_vets(self):
        "return number of vets"
        return len(self.get_vets())

    def _get_list_id_farriers(self):
        "return farrier id"
        self.dtb.cur.execute("""select TContact.id From TContact
            where TContact.is_farrier=1 ORDER BY TContact.last_name""")
        list_id_farriers = self.dtb.cur.fetchall()
        return [item[0] for item in list_id_farriers]

    def get_farriers(self):
        "return farriers"
        list_id_farriers = self._get_list_id_farriers()
        return [Contact(self.dtb, i) for i in list_id_farriers]

    def get_nb_farriers(self):
        "return number of farriers"
        return len(self.get_farriers())
