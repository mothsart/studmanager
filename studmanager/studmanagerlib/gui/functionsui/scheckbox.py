#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QCheckBox
"""

from PyQt5.QtWidgets import QCheckBox

__all__ = ['SCheckBoxDone', 'SCheckBoxFarrier', 'SCheckBoxVet']

# QCheckBox


class SCheckBoxDone(QCheckBox):
    def __init__(self, value=False):
        QCheckBox.__init__(self)
        self.setText("Done")
        self.set(value)

    def set(self, value):
        self.setChecked(value)

    def get(self):
        return self.isChecked()


class SCheckBoxFarrier(QCheckBox):
    def __init__(self, value=False):
        QCheckBox.__init__(self)
        self.setText("Farrier")
        self.set(value)

    def set(self, value):
        self.setChecked(value)

    def get(self):
        return self.isChecked()


class SCheckBoxVet(QCheckBox):
    def __init__(self, value=False):
        QCheckBox.__init__(self)
        self.setText("Vet/Care taker")
        self.set(value)

    def set(self, value):
        self.setChecked(value)

    def get(self):
        return self.isChecked()
