#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class to describe Contacts
"""

__all__ = ['Contact']


class Contact():
    " Class to represent Contact"

    def __init__(self, dtb, id_contact):
        self.dtb_id = id_contact
        self.dtb = dtb

    def get_last_name(self):
        "return contact last name"
        self.dtb.cur.execute('''select TContact.last_name from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        last_name = self.dtb.cur.fetchone()[0]
        return last_name

    def get_first_name(self):
        "return contact fisrt name"
        self.dtb.cur.execute('''select TContact.first_name from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        first_name = self.dtb.cur.fetchone()[0]
        return first_name

    def get_short_name(self):
        "return last name + fisrt name"
        return "{} {}".format(self.get_last_name(), self.get_first_name())

    def get_full_name(self):
        "return last name + fisrt name + (organization)"
        if self.get_organization() == "":
            return self.get_short_name()
        else:
            return "{} {} ({})".format(self.get_last_name(),
                                       self.get_first_name(),
                                       self.get_organization())

    def get_address(self):
        "return contact address"
        self.dtb.cur.execute('''select TContact.address from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        address = self.dtb.cur.fetchone()[0]
        return address

    def get_tel(self):
        "return contact phone number"
        self.dtb.cur.execute('''select TContact.tel from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        tel = self.dtb.cur.fetchone()[0]
        return tel

    def get_email(self):
        "return contact email"
        self.dtb.cur.execute('''select TContact.email from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        email = self.dtb.cur.fetchone()[0]
        return email

    def is_vet(self):
        "return 0/1 for not vet/vet"
        self.dtb.cur.execute('''select TContact.is_vet from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        isvet = self.dtb.cur.fetchone()[0]
        return bool(isvet)

    def is_farrier(self):
        "return 0/1 for not farrier/farrier"
        self.dtb.cur.execute('''select TContact.is_farrier from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        isfarrier = self.dtb.cur.fetchone()[0]
        return bool(isfarrier)

    def get_organization(self):
        "return contact organization"
        self.dtb.cur.execute('''select TContact.organization from TContact
                            where TContact.id= ? ''', (self.dtb_id, ))
        organization = self.dtb.cur.fetchone()[0]
        return organization

    def __repr__(self):
        return self.get_full_name()
