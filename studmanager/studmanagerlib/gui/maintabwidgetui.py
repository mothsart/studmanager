#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    class the main tab widget
"""

from PyQt5.QtWidgets import (QTabWidget)
from PyQt5.QtGui import QIcon

from studmanagerlib.gui.maintabsui import WidgetContact, WidgetDashboard
from studmanagerlib.gui.maintabsui import WidgetMyHorses, WidgetReminderRecords
from studmanagerlib.constants import ICON_PATH

__all__ = ['MainTabWidget']

class MainTabWidget(QTabWidget):
    def __init__(self, studb):
        QTabWidget.__init__(self)
        self.dtb = studb


        # == Main window tabs ==
        # Initialize tab screen
        self.tab_dashboard = WidgetDashboard(self.dtb)
        self.tab_my_horses = WidgetMyHorses(self.dtb)
        self.tab_contacts = WidgetContact(self.dtb)
        self.tab_reminders = WidgetReminderRecords(self.dtb)
#        self.tab_reports = QWidget()

        # Add tabs
        self.addTab(self.tab_dashboard, "Dashboard")
        self.addTab(self.tab_my_horses, "My Horses")
        self.addTab(self.tab_contacts, "Contacts")
        self.addTab(self.tab_reminders, "Reminders")
        # self.main_tabs.addTab(self.tab_reports,"Reports")

        # tab icon
        self.setTabIcon(0, QIcon.fromTheme("go-home"))
        self.setTabIcon(1, QIcon(ICON_PATH + "horse.svg"))
        self.setTabIcon(2, QIcon.fromTheme("x-office-address-book"))
        self.setTabIcon(3, QIcon.fromTheme("task-due"))
#        self.main_tabs.setTabIcon(4, QIcon.fromTheme("accessories-dictionary"))
