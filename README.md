# Stud Manager

This software is a tool designed in to help Horse Stud Manager to take care of their horses.


It has the following features :

  * Health Records
  * Farrier Records
  * Movement Records
  * Address Book
  * Reminders
  * Cross platform software

Enjoy and have fun ;-)

## Installation

To use it, you need to install on your computer Python3 and PyQt5.

For example, on Ubuntu

`sudo apt install python3 python3-pyqt5 sqlite3`

Then, you can start this software by executing the script "setup.py".

`python3 main.py`

## Licence

This software is distributed under GPL v3 or later.

## Changelog

 * Version 0.1.0 (2018/08/09) : first version

