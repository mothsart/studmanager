# Maintainer

 * toitoinebzh : since 2018

# Main Authors

 * toitoinebzh : created this software in 2018

# Code Contributors

# Contributors

 * [Ubuntu-fr.org](https://ubuntu-fr.org/) : bipede, kikito, Pimprelune, moths-art
 * [Developpez.com](https://www.developpez.net) : VinsS
 
# Graphics
 
 * Thanks to [openclipart.org](https://openclipart.org/) for providing severals icons used in this software. 
      * horse.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/6163/chess-horse
    
     * horse_shoe.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/215358/horse-shoe
    
     * syringe.svg
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/205886/syringe
    
     * movements.ico
         * Licence : CC0 1.0 Universal (CC0 1.0)
         * https://openclipart.org/detail/27557/iso-truck-3