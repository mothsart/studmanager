#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    StudManager is a software designed to help Horse Stud Manager.
    This package contains functions and classes to use StudManager.
"""

#__version__ = "0.1"

#from core import proclamer
#from StudManagerLib import *
# import .StudManagerLib

from . import studmanagerlib
from .main import studmanager

__version__ = studmanagerlib.__version__
