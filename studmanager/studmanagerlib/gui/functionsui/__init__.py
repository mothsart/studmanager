#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
	contains ui fonctions
"""

#from core import proclamer
from .studerrormessage import StudErrormessage
#from .MainTabsUI import *
#from . import MainTabsUI

from .scheckbox import SCheckBoxDone, SCheckBoxFarrier, SCheckBoxVet
from .scombobox import SComboBoxBreed, SComboBoxColor, SComboBoxSex, SComboBoxHorse, SComboBoxVet, SComboBoxFarrier, SComboBoxContact
from .sdateedit import SDateEdit
from .slabel import (
    SLabelName,
    SLabelBirthdate,
    SLabelBreed,
    SLabelColor,
    SLabelSex,
    SLabelDate,
    SLabelHorse,
    SLabelComments,
    SLabelVet,
    SLabelFarrier,
    SLabelLastName,
    SLabelFirstName,
    SLabelOrganization,
    SLabelEmail,
    SLabelTel,
    SLabelAddress,
    SLabelJob)
from .splaintextedit import SPlainTextEditComments
from .spushbutton import SPushButtonAdd, SPushButtonEdit, SPushButtonDelete, SPushButtonUpdate
