#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class for the Horses tab UI
"""

from PyQt5.QtWidgets import (QLabel, QGridLayout, QWidget, QTabWidget,
                             QTreeWidget, QTreeWidgetItem)
from PyQt5.QtGui import QIcon

from studmanagerlib.gui.maintabsui.myhorsetab import (
    WidgetDescriptionHorse, WidgetFarrierRecords, WidgetHealthRecords,
    WidgetMovementRecords)
from studmanagerlib.functions import reverse_dict
from studmanagerlib.gui.functionsui import SPushButtonAdd
from studmanagerlib.database import Horse
from studmanagerlib.constants import ICON_PATH

__all__ = ['WidgetMyHorses']


class TabMyHorse(QTabWidget):
    "widget that contains all tabs about one horse"
    def __init__(self, studdb, horse = None):
        QTabWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse
    
        # Initialize tab screen
        
        self.tab_description = WidgetDescriptionHorse(self.dtb, self.horse)
        self.tab_health_records = WidgetHealthRecords(self.dtb, self.horse)
        self.tab_movements = WidgetMovementRecords(self.dtb, self.horse)
        self.tab_farrier = WidgetFarrierRecords(self.dtb, self.horse)

        # Add tabs
        self.addTab(self.tab_description, "Description")
        self.addTab(self.tab_health_records, "Health Records")
        self.addTab(self.tab_movements, "Movements")
        self.addTab(self.tab_farrier, "Farrier")

        # tab icons
        self.setTabIcon(0, QIcon.fromTheme("emblem-documents"))
        self.setTabIcon(1, QIcon(ICON_PATH + "syringe.svg"))
        self.setTabIcon(2, QIcon(ICON_PATH + "movements.svg"))
        self.setTabIcon(3, QIcon(ICON_PATH + "horse_shoe.svg"))
        
        
    def set_horse(self, horse):
        "update all the tabs"
        self.horse = horse
        if self.horse is not None:
            self.tab_description.set_horse(self.horse)
            self.tab_health_records.set_horse(self.horse)
            self.tab_movements.set_horse(self.horse)
            self.tab_farrier.set_horse(self.horse)
        else:
            self.hide() ## bug !!!
        

        
class ListHorse(QTreeWidget):
    def __init__(self, studb):
        QTreeWidget.__init__(self)
        self.dtb = studb

        self.setColumnCount(1)
        self.setHeaderLabels(["Name"])
       
        self.view_dict_in= {}
        self.view_dict_out= {}
        self.in_items = QTreeWidgetItem(["In"])
        self.out_items = QTreeWidgetItem(["Out"])
        
        self.refresh()
        self.expandAll()

    def refresh(self):
        
        # to save qtreeview expanded state
        in_items_is_expanded = self.in_items.isExpanded()
        out_items_is_expanded = self.out_items.isExpanded()
            
        # to refresh the list        
        self.clear()
        self.view_dict_in= {}
        self.view_dict_out= {}
        
        self.in_items = QTreeWidgetItem(["In"])
        self.out_items = QTreeWidgetItem(["Out"])
          
        row_in=0
        row_out=0
        for row, horse in enumerate(self.dtb.table_horse.get_horses()):
            data=QTreeWidgetItem([horse.get_name()])
            if horse.is_in():
                self.in_items.addChild(data)
                self.view_dict_in[horse.dtb_id] = row_in
                row_in+=1
            else :
                self.out_items.addChild(data)
                self.view_dict_out[horse.dtb_id] = row_out
                row_out+=1
        
        self.addTopLevelItem(self.in_items)
        self.addTopLevelItem(self.out_items)

        # to restore Qtreeview expanded state
        self.in_items.setExpanded(in_items_is_expanded)
        self.out_items.setExpanded(out_items_is_expanded)
            
    def get_selection(self):
        row = self.currentIndex().row()  # row
        cat = self.currentIndex().parent().row()  # in/out
        if row == -1 or cat == -1:
            return None
        else:
            if cat == 0 or cat == 1:
                if cat == 0:
                    id_horse = reverse_dict(self.view_dict_in, row)
                else:
                    id_horse = reverse_dict(self.view_dict_out, row)
                horse = Horse(self.dtb, id_horse)
                return horse
            else:
                pass
        
class WidgetMyHorses(QWidget):
    "widget that define what is inside my horses tab"

    def __init__(self, studdb, horse = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        # == My horses page ==

        # #########
        # left side
        # #########
        
        self.but_add_horse = SPushButtonAdd("Add Horse")
        self.but_add_horse.clicked.connect(self.create_horse)
        
        self.dataview = ListHorse(self.dtb)
        self.dataview.clicked.connect(self.on_horse_selected)

        # #########
        # right side
        # #########
        self.label_horse_name = QLabel("<h1> </h1>")
        
        self.tabs = TabMyHorse(self.dtb, self.horse)


        # == trigger ==
        self.tabs.tab_description.but_save_changes.clicked.connect(self.dataview.refresh)
        self.tabs.tab_description.line_name.textChanged.connect(self.update_horse_name_title)
        self.tabs.tab_movements.dataview.refreshed.connect(self.dataview.refresh)
        
        # add treeview and tabs to tabMyHorses
        gridlayout = QGridLayout(self)

        # add left and right side in tabMyHorses
        # add treeview and tabs to tabMyHorses
        gridlayout.addWidget(self.but_add_horse, 0, 0, 1, 1)
        gridlayout.addWidget(self.dataview, 1, 0, 9, 3)
        gridlayout.addWidget(self.label_horse_name, 0, 3, 1, 7)
        gridlayout.addWidget(self.tabs, 1, 3, 9, 7)
        # self.gridlayout_tabs.addWidget(self.tabs,0,3,10,7)
        for i in range(10):
            gridlayout.setColumnStretch(i, 1)

    def update_horse_name_title(self):
        "update the horse name title"
        horse_name = self.tabs.tab_description.line_name.text()
        self.label_horse_name.setText("<h1>" + horse_name + "</h1>")

    def on_horse_selected(self):
        "update display when a horse is selected"
        
        horse = self.dataview.get_selection()
        if horse is not None :
            self.set_horse(horse)
        

    def set_horse(self, horse):
        "update all the tabs"
        self.horse = horse
        self.tabs.set_horse(horse)

    def create_horse(self):
        "action to create a new horse"
        
        nb_horse = self.dtb.table_horse.get_nb_horses()

        name = "New horse " + str(nb_horse)
        # print(name)
        id_sex = 1
        id_color = 1
        birthdate = "2000-01-01"
        id_breed = 1
        horse_id = self.dtb.table_horse.add_horse(name, id_sex, id_color,
                                                  birthdate, id_breed)
        self.dataview.refresh()
        horse=Horse(self.dtb, horse_id)
        self.set_horse(horse)
