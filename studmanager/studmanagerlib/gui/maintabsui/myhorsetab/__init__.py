#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	this module contains each tabs to describe one horse
"""

#__version__ = "0.1"

#from core import proclamer

from .descriptionhorseui import WidgetDescriptionHorse
from .farrierrecordsui import WidgetFarrierRecords, InputFarrierRecord
from .healthrecordsui import WidgetHealthRecords, InputHealthRecord
from .movementrecordsui import WidgetMovementRecords, InputMovementRecord
