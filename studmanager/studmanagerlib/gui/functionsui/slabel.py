#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QLabel
"""

from PyQt5.QtWidgets import QLabel

__all__ = [
    'SLabelName',
    'SLabelBirthdate',
    'SLabelBreed',
    'SLabelColor',
    'SLabelSex',
    'SLabelDate',
    'SLabelHorse',
    'SLabelComments',
    'SLabelVet',
    'SLabelFarrier',
    'SLabelLastName',
    'SLabelFirstName',
    'SLabelOrganization',
    'SLabelEmail',
    'SLabelTel',
    'SLabelAddress',
    'SLabelJob']

# QLabel


class SLabelName(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Name : ")


class SLabelBirthdate(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Birthdate : ")


class SLabelBreed(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Breed : ")


class SLabelColor(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Color : ")


class SLabelSex(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Sex : ")


class SLabelDate(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Date : ")


class SLabelHorse(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Horse : ")


class SLabelComments(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Comments : ")


class SLabelVet(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Vet/Care taker : ")


class SLabelFarrier(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Farrier : ")


class SLabelLastName(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Last Name : ")


class SLabelFirstName(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("First Name : ")


class SLabelOrganization(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Organization : ")


class SLabelEmail(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Email : ")


class SLabelTel(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Tel : ")


class SLabelAddress(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Address : ")


class SLabelJob(QLabel):
    def __init__(self):
        QLabel.__init__(self)
        self.setText("Job : ")
