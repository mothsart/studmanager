#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QComboBox
"""

from PyQt5.QtWidgets import QComboBox
from studmanagerlib.database import Contact, Breed, Color, Horse, Sex

__all__ = [
    'SComboBoxBreed',
    'SComboBoxColor',
    'SComboBoxSex',
    'SComboBoxHorse',
    'SComboBoxVet',
    'SComboBoxFarrier',
    'SComboBoxContact']

# QComboBox


class SComboBoxBreed(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_breed.get_breeds()):
            self.addItem(data.get_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Breed(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxColor(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_color.get_colors()):
            self.addItem(data.get_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Color(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxSex(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_sex.get_sexes()):
            self.addItem(data.get_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Sex(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxHorse(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_horse.get_horses()):
            self.addItem(data.get_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Horse(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxVet(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_contact.get_vets()):
            self.addItem(data.get_full_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Contact(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxFarrier(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_contact.get_farriers()):
            self.addItem(data.get_full_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Contact(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)


class SComboBoxContact(QComboBox):
    def __init__(self, studdb, obj=None):
        QComboBox.__init__(self)
        self.dtb = studdb
        self.combobox_dict = {}
        for row, data in enumerate(studdb.table_contact.get_contacts()):
            self.addItem(data.get_full_name(), data.dtb_id)
            self.combobox_dict[data.dtb_id] = row
        self.set_obj(obj)

    def get_id(self):
        return self.itemData(self.currentIndex())

    def get_obj(self):
        obj_id = self.get_id()
        return Contact(self.dtb, obj_id)

    def set_id(self, obj_id):
        row = self.combobox_dict[obj_id]
        self.setCurrentIndex(row)

    def set_obj(self, obj):
        if obj is not None:
            self.set_id(obj.dtb_id)
