#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    class StudWindow is the class used to handle the main window
"""

#import os
import sys
import gettext

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import (QMainWindow, QAction, QGridLayout, QWidget,
                             QStatusBar, QFileDialog, QMessageBox, QStyle)
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon

from PyQt5.QtCore import (qInstallMessageHandler)

import studmanagerlib
from studmanagerlib.database import StudDb
from studmanagerlib.gui import MainTabWidget
from studmanagerlib.gui.functionsui import StudErrormessage
from studmanagerlib.constants import ICON_DIR, LICENSE_PATH, ICON_PATH


gettext.install('studmanager', './locale')

__all__ = ['StudWindow']

class StudWindow(QMainWindow):
    "main window class"

    def __init__(self):
        QMainWindow.__init__(self)

        self.setMinimumSize(QSize(1.5 * 640, 1.5 * 480))
        self.setWindowTitle("Stud Manager")
        self.setWindowIcon(QIcon(ICON_PATH + "horse.svg"))
        
        qInstallMessageHandler(StudErrormessage)

        self.dtb = None

        if sys.platform == 'win32':
            QIcon.setThemeName("gnome")
            QIcon.setThemeSearchPaths([ICON_DIR])


        self.create_menu()

        # status bar
        self.status_bar = QStatusBar()
        self.setStatusBar(self.status_bar)
        self.status_bar.showMessage("Welcome in Stud Manager", 2000)

        #self.set_filename("sample/demo.sqlite")

    def init_ui(self):
        "init ui"

        central_widget = QWidget(self)
        self.gridlayout = QGridLayout(central_widget)

        self.main_tabs = MainTabWidget(self.dtb)
        self.gridlayout.addWidget(self.main_tabs)
        
        self.setCentralWidget(central_widget)


    def create_menu(self):
        "create menu and toolbar"
        # ========  menu =============
        menu_bar = self.menuBar()

        # = File

        # Create new action
        new_action = QAction(QIcon.fromTheme("document-new"), '&New', self)
        new_action.setShortcut('Ctrl+N')
        new_action.setStatusTip(_('New document'))
        new_action.triggered.connect(self.new_call)

        # Create new action
        open_action = QAction(QIcon.fromTheme("document-open"), '&Open', self)
        open_action.setShortcut('Ctrl+O')
        open_action.setStatusTip('Open document')
        open_action.triggered.connect(self.open_call)

        # Create new action
        save_action = QAction(QIcon.fromTheme("document-save"), '&Save', self)
        save_action.setShortcut('Ctrl+S')
        save_action.setStatusTip('Save document')
        save_action.triggered.connect(self.save_call)

        # Create new action
        close_action = QAction(QIcon.fromTheme("window-close"), '&Close', self)
        close_action.setShortcut('Ctrl+W')
        close_action.setStatusTip('Close document')
        close_action.triggered.connect(self.close_call)

        # Create exit action
        exit_action = QAction(
            QIcon.fromTheme("application-exit"), '&Exit', self)
        exit_action.setShortcut('Ctrl+Q')
        exit_action.setStatusTip('Exit application')
        exit_action.triggered.connect(self.exit_call)

        # Create menu bar and add action
        file_menu = menu_bar.addMenu('&File')
        file_menu.addAction(new_action)
        file_menu.addAction(open_action)
        file_menu.addAction(save_action)
        file_menu.addAction(close_action)
        file_menu.addAction(exit_action)

        # = Help
        # Create new action
        about_action = QAction(
            QIcon.fromTheme("help-about"), 'About Stud Manager', self)
        # about_action.setShortcut('Ctrl+P')
        about_action.setStatusTip('About Stud Manager')
        about_action.triggered.connect(self.about_call)

        # Create new action
        about_qt_action = QAction(self.style().standardIcon(
            QStyle.SP_TitleBarMenuButton), 'About Qt', self)
        # about_qt_action.setShortcut('Ctrl+P')
        about_qt_action.setStatusTip('About Qt')
        about_qt_action.triggered.connect(self.about_qt_call)

        help_menu = menu_bar.addMenu('&Help')
        help_menu.addAction(about_action)
        help_menu.addAction(about_qt_action)

        # ========  toolbar =============
        toolbar = self.addToolBar('Toolbar')
        # toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        toolbar.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        toolbar.setMovable(False)
        toolbar.addAction(new_action)
        toolbar.addAction(open_action)
        toolbar.addAction(save_action)

    def new_call(self):
        "to start a new file"
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getSaveFileName(
            self,
            "New file",
            "",
            "Sqlite Files (*.sqlite);;All Files (*)",
            options=options)
        if file_name:
            self.set_filename(file_name)

    def open_call(self):
        "to open a file"
        options = QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(
            self,
            "Open file",
            "",
            "Sqlite Files (*.sqlite);;All Files (*)",
            options=options)
        if file_name:
            self.set_filename(file_name)

    def save_call(self):
        "to save file"
        if hasattr(self, "dtb"):
            self.dtb.save()
            self.status_bar.showMessage("File saved successfully", 2000)
        else:
            self.status_bar.showMessage("No file to save !", 2000)

    def close_call(self):
        "to close file"
        if hasattr(self, "db"):
            self.dtb.close()
            del self.dtb
            self.status_bar.showMessage("File closed successfully", 2000)
        else:
            self.status_bar.showMessage("No file to close !", 2000)
        
        if hasattr(self, "main_tabs"):
            #self.main_tabs.setHidden(True)
            self.main_tabs.deleteLater()

    def exit_call(self):
        "to close the window"
        self.close()

    def about_call(self):
        "about dialog"
        licence_file = open(LICENSE_PATH, "r")
        licence = licence_file.read()
        licence_file.close()

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)

        msg.setText(
            "Stud Manager {} \nStud Manager is a software designed to help you to manage your stud.".format(
                studmanagerlib.__version__))
        msg.setInformativeText(
            """This software has been written with Python, PyQt5 and sqlite."""
        )
        msg.setWindowTitle("About Stud Manager")
        msg.setDetailedText(licence)
        msg.setStandardButtons(QMessageBox.Ok)
        # msg.buttonClicked.connect(msgbtn)

        msg.exec_()

    def about_qt_call(self):
        "about qt dialog"
        QMessageBox.aboutQt(self, "About Qt")

    def set_filename(self, filename):
        "to set the file to read"

        self.dtb = StudDb(filename)

        self.init_ui()

if __name__ == "__main__":
    APP = QtWidgets.QApplication(sys.argv)
    MAINWIN = StudWindow()
    MAINWIN.show()
    sys.exit(APP.exec_())
