#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    this file contains basic functions
"""

__all__ = ['reverse_dict']


def reverse_dict(my_map, value):
    " return key of a dict from the value (my_map[key]=value)"
    inv_map = {v: k for k, v in my_map.items()}
    key = inv_map[value]
    return key
