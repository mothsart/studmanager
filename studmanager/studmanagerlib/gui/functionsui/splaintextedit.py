#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Predefined QCheckBox
"""

from PyQt5.QtWidgets import QPlainTextEdit

__all__ = ['SPlainTextEditComments']


# QPlainTextEdit

class SPlainTextEditComments(QPlainTextEdit):
    def __init__(self, comments=None):
        QPlainTextEdit.__init__(self)
        self.set_comments(comments)

    def set_comments(self, comments):
        if comments is not None:
            self.setPlainText(comments)
        else:
            self.setPlainText("Add comments...")

    def get_comments(self):
        return self.toPlainText()
