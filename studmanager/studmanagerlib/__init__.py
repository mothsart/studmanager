#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
	contains class and functions for StudManager
"""

__version__ = "0.1.0"


from . import database
from . import gui
from . import functions
from . import constants


#from core import proclamer
#from .Database import *
#from .Functions import *
#from .Gui import *
#from .Gui.MainTabsUI import *
#from .Gui.MainTabsUI.MyHorseTab import *
