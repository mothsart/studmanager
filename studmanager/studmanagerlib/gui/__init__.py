#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
	this module contains gui and fonctions and mainwindow ui
"""

#from core import proclamer
from .maintabwidgetui import MainTabWidget
from .mainwindowui import StudWindow
#from .MainTabsUI import *
from . import maintabsui
from . import functionsui
