#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    contains UI class to track movement of horse
"""

from PyQt5.QtWidgets import (QDialog, QMessageBox, QDialogButtonBox,
                             QGridLayout, QWidget, 
                             QTreeWidget, QTreeWidgetItem)
from PyQt5.QtCore import pyqtSignal

from studmanagerlib.functions import reverse_dict
from studmanagerlib.database import MovementRecord
from studmanagerlib.gui.functionsui import (
    SPushButtonAdd,
    SPushButtonEdit,
    SPushButtonDelete,
    SDateEdit,
    SLabelDate,
    SPlainTextEditComments,
    SLabelComments)

__all__ = ['WidgetMovementRecords', 'InputMovementRecord']

def event_type(horse, row):
    "return if the event is IN or OUT"
    if horse.is_in():
        in_out_int = 0
    else:
        in_out_int = 1

    if row % 2 == in_out_int:
        event = "In"
    else:
        event = "Out"
        
    return event


class WidgetMovementRecords(QWidget):
    "widget inside helath record tab"

    def __init__(self, studdb, horse = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse

        # == TabHealthRecords tab ==
        but_add = SPushButtonAdd("Add Movement record")
        but_add.clicked.connect(self.create_movement_record)

        but_edit = SPushButtonEdit("Edit Movement record")
        but_edit.clicked.connect(self.edit_movement_record)

        but_delete = SPushButtonDelete("Delete Movement record")
        but_delete.clicked.connect(self.delete_movement_record)

        self.dataview = ListMovementRecord(self.dtb, self.horse)
        self.dataview.doubleClicked.connect(self.edit_movement_record)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(but_add, 0, 0)
        gridlayout.addWidget(but_edit, 0, 1)
        gridlayout.addWidget(but_delete, 0, 2)
        gridlayout.addWidget(self.dataview, 1, 0, 1, 3)

    def create_movement_record(self):
        "create/edit a movement record"
        movement_record = None

        input_data = InputMovementRecord(self.dtb, movement_record)

        if input_data.exec_() == QDialog.Accepted:
            date_movement_record, comments = input_data.get_infos()

            self.dtb.table_movement_records.add_movement_record(
                date_movement_record, self.horse.dtb_id, comments)
            self.dataview.refresh()
        else:
            pass

    def edit_movement_record(self):
        movement_record = self.dataview.get_selection()
        if movement_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one movement record to edit.",
                QMessageBox.Ok)
        else:
            input_data = InputMovementRecord(self.dtb, movement_record)

            if input_data.exec_() == QDialog.Accepted:
                date_movement_record, comments = input_data.get_infos()

                self.dtb.table_movement_records.update_movement_record(
                    movement_record.dtb_id, date_movement_record, self.horse.dtb_id, comments)
                self.dataview.refresh()
            else:
                pass

    def delete_movement_record(self):
        "delete movement record"
        movement_record = self.dataview.get_selection()
        if movement_record is None:
            QMessageBox.warning(
                self, 'Error !',
                "You have to select one movement record to delete.",
                QMessageBox.Ok)
        else:
            self.dtb.table_movement_records.delete_movement_record(
                movement_record.dtb_id)
            self.dataview.refresh()
            
    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        self.dataview.set_horse(self.horse)


class ListMovementRecord(QTreeWidget):
    refreshed = pyqtSignal()
    def __init__(self, studb, horse=None):
        QTreeWidget.__init__(self)
        self.dtb = studb
        self.horse = horse

        self.setColumnCount(3)
        self.setHeaderLabels(["Date", "Event", "Comments"])
        self.view_dict = {}
        self.set_horse(self.horse)

    def refresh(self):
        self.clear()
        self.view_dict = {}
        for row, movement_record in enumerate(
                self.horse.get_movement_records()):
            date_movement_record = str(movement_record.get_date())
            event =event_type(self.horse, row)
            comments = movement_record.get_comments()
            data = QTreeWidgetItem(
                [date_movement_record, event, comments])
            self.addTopLevelItem(data)
            self.view_dict[movement_record.dtb_id] = row
                
        self.refreshed.emit()

    def get_selection(self):
        row = self.currentIndex().row()
        if row == -1:
            return None
        else:
            id_movement_record = reverse_dict(self.view_dict, row)
            return MovementRecord(self.dtb, id_movement_record)

    def set_horse(self, horse):
        "set horse to display"
        self.horse = horse
        if self.horse is not None:
            self.refresh()
        
class InputMovementRecord(QDialog):
    "dialog to create/edit movement records"
#        self.test_widget=InputHealthRecord(dtb, hr)
#        if self.test_widget.exec_() == QDialog.Accepted:
#            print(self.test_widget.get_infos())

    def __init__(self, studdb, movement_record=None):
        QDialog.__init__(self)
        self.setWindowTitle("Movement Record")
        self.dtb = studdb

        # data to display
        if movement_record is None:
            date = None
            #vet = None
            comments = None
        else:
            date = movement_record.get_date()
            #vet = movement_record.get_vet()
            comments = movement_record.get_comments()

        # widget
        date_label = SLabelDate()
        self.date_edit = SDateEdit(date)

        #label = SLabelVet()
        #self.combobox = SComboBoxVet(self.dtb, vet)

        comments_label = SLabelComments()
        self.plain_text_edit_comments = SPlainTextEditComments(comments)

        button_box = QDialogButtonBox(self)
        button_box.setStandardButtons(
            QDialogButtonBox.Save | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        # layout
        gridlayout = QGridLayout(self)

        gridlayout.addWidget(date_label, 0, 0)
        gridlayout.addWidget(self.date_edit, 0, 1)

        #gridlayout.addWidget(label, 1, 0)
        #gridlayout.addWidget(self.combobox, 1, 1)

        gridlayout.addWidget(comments_label, 1, 0)
        gridlayout.addWidget(self.plain_text_edit_comments, 1, 1)

        gridlayout.addWidget(button_box, 2, 1)
        # self.exec_()

    def get_infos(self):
        "return infos from dialog"
        date_movement_record = self.date_edit.get()
        #vet = self.combobox.get_obj()
        comments = self.plain_text_edit_comments.get_comments()
        return date_movement_record, comments
