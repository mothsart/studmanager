#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    class UI to describe and update Horse
"""

from PyQt5.QtWidgets import (QGridLayout, QWidget, QLineEdit)
from PyQt5.QtCore import QDate


from studmanagerlib.gui.functionsui import (
    SLabelName,
    SLabelBirthdate,
    SLabelBreed,
    SLabelSex,
    SLabelColor,
    SComboBoxBreed,
    SComboBoxColor,
    SComboBoxSex,
    SPushButtonUpdate,
    SDateEdit)

__all__ = ['WidgetDescriptionHorse']


class WidgetDescriptionHorse(QWidget):
    "widget to describe a horse"

    def __init__(self, studdb, horse = None):
        QWidget.__init__(self)
        self.dtb = studdb
        self.horse = horse
        
        # == Description tab ==
        label_name = SLabelName()
        self.line_name = QLineEdit()
        
        label_birthdate = SLabelBirthdate()
        self.dob = SDateEdit()

        label_breed = SLabelBreed()
        self.cb_breed = SComboBoxBreed(self.dtb)

        label_color = SLabelColor()
        self.cb_color = SComboBoxColor(self.dtb)

        label_sex = SLabelSex()
        self.cb_sex =SComboBoxSex(self.dtb)

        self.but_save_changes = SPushButtonUpdate("Update")
        self.but_save_changes.clicked.connect(self.save_changes)

        gridlayout = QGridLayout(self)
        gridlayout.addWidget(label_name, 0, 0, 1, 1)
        gridlayout.addWidget(self.line_name, 0, 1, 1, 3)
        gridlayout.addWidget(label_birthdate, 1, 0, 1, 1)
        gridlayout.addWidget(self.dob, 1, 1, 1, 1)
        gridlayout.addWidget(label_sex, 1, 2, 1, 1)
        gridlayout.addWidget(self.cb_sex, 1, 3, 1, 1)
        gridlayout.addWidget(label_color, 2, 0, 1, 1)
        gridlayout.addWidget(self.cb_color, 2, 1, 1, 1)
        gridlayout.addWidget(label_breed, 2, 2, 1, 1)
        gridlayout.addWidget(self.cb_breed, 2, 3, 1, 1)
        gridlayout.addWidget(self.but_save_changes, 3, 3, 1, 1)

        for i in range(4):
            gridlayout.setColumnStretch(i, 1)

        for i in range(5):
            gridlayout.setRowStretch(i, 1)

        self.set_horse(self.horse)
        
    def save_changes(self):
        "update horse data"
        name = self.line_name.text()
        breed = self.cb_breed.get_obj()
        color = self.cb_color.get_obj()
        sex = self.cb_sex.get_obj()

        birthdate = self.dob.get()


        self.dtb.table_horse.update_horse(self.horse.dtb_id, name, sex.dtb_id,
                                          color.dtb_id, birthdate, breed.dtb_id)
    
    def set_horse(self, horse = None):
        "set horse to display"
        self.horse = horse
        # set horse in description tab
        if self.horse is not None :
            self.line_name.setText(self.horse.get_name())
            dob = self.horse.get_birthdate()
            self.dob.set(QDate(dob.year, dob.month, dob.day))
            
            self.cb_breed.set_obj(self.horse.get_breed())
            self.cb_color.set_obj(self.horse.get_color())
            self.cb_sex.set_obj(self.horse.get_sex())